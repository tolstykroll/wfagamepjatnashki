﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace wfaGamePjatnashki
{
    public partial class Form1 : Form
    {
        Button[] B;

        // Implement Timer in game with buttons
        System.Timers.Timer timer;
        int hours = 0, minutes = 0, seconds = 0;

        public Form1()
        {
            InitializeComponent();
            B = new Button[16];
            B[0] = button1;
            B[1] = button2;
            B[2] = button3;
            B[3] = button4;
            B[4] = button5;
            B[5] = button6;
            B[6] = button7;
            B[7] = button8;
            B[8] = button9;
            B[9] = button10;
            B[10] = button11;
            B[11] = button12;
            B[12] = button13;
            B[13] = button14;
            B[14] = button15;
            B[15] = button16;

            panel1_Resize(null, null);
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            for (int i = 0; i < 16; i++)
            {
                int x, y;
                y = i / 4;
                x = i - y * 4;
                B[i].Top = y * panel1.Height / 5;
                B[i].Left = x * panel1.Width / 4;
                B[i].Width = panel1.Width / 4;
                B[i].Height = panel1.Height / 5;
                B[i].Font = new Font("Arial", panel1.Height / 10, FontStyle.Bold);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btnClicked = (Button)sender;
            int n = (int) Convert.ToInt64(btnClicked.Tag.ToString());

            n--;

            int y = n / 4;
            int x = n - y * 4;

            int yTop, yBottom;
            int xLeft, xRight;

            yTop = y - 1;
            yBottom = y + 1;
            xLeft = x - 1;
            xRight = x + 1;

            // Move right
            if (xRight < 4)
            {
                int nr = y * 4 + xRight;
                if (!B[nr].Visible)
                {
                    B[nr].Visible = true;
                    B[n].Visible = false;
                    B[nr].Text = B[n].Text;
                }
            }
            // Move left
            if (xLeft >= 0)
            {
                int nl = y * 4 + xLeft;
                if (!B[nl].Visible)
                {
                    B[nl].Visible = true;
                    B[n].Visible = false;
                    B[nl].Text = B[n].Text;
                }
            }
            // Move top
            if (yTop >= 0)
            {
                int nt = yTop * 4 + x;
                if (!B[nt].Visible)
                {
                    B[nt].Visible = true;
                    B[n].Visible = false;
                    B[nt].Text = B[n].Text;
                }
            }
            // Move bottom
            if (yBottom >= 0)
            {
                int nb = yBottom * 4 + x;
                if (!B[nb].Visible)
                {
                    B[nb].Visible = true;
                    B[n].Visible = false;
                    B[nb].Text = B[n].Text;
                }
           
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000; // 1 sec
            timer.Elapsed += OnTimeEvents;
        }

        private void OnTimeEvents(object sender, ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                seconds += 1;
                if (seconds == 60)
                {
                    seconds = 0;
                    minutes += 1;
                }
                if (minutes == 60)
                {
                    minutes = 0;
                    hours += 1;
                }
                txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));
            }));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
            Application.DoEvents();
        }

        private void btnTimerStart_Click(object sender, EventArgs e)
        {
            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));

            timer.Start();
        }

        private void btnTimerStop_Click(object sender, EventArgs e)
        {
            hours = 0;
            minutes = 0;
            seconds = 0;

            txtTime.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'),
                                                            minutes.ToString().PadLeft(2, '0'),
                                                            seconds.ToString().PadLeft(0, '0'));

            timer.Stop();
        }
    }
}
